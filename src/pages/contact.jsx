import React from "react"; //ini gak wajib, cuma klo pakai properti dari react, wajib pakai.
import "./contact.css";
import NavbarZ from "../component/navbar"
import Footer from "../component/footer"
import { Form, Button } from 'react-bootstrap';

function Contactme() {
    return (
        <>
        <div className="contact">
        <NavbarZ/>
        <Form >
            <Form.Group controlId="formBasicType">
                <Form.Label>First Name </Form.Label>
                <Form.Control type="type" placeholder="John" />
                <Form.Label> Last Name </Form.Label>
                <Form.Control type="type" placeholder="Doe" />
            </Form.Group>
            <br/>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address </Form.Label>
                <Form.Control type="email" placeholder="random@mail.com" />
                <Form.Text style={{color: "white"}} className="text-muted">
                </Form.Text>
            </Form.Group>
            <br/>
            <Form.Group controlId="exampleForm.ControlTextarea1">
                <Form.Label></Form.Label>
                <Form.Control as="textarea" cols={55} rows={5} placeholder="Input your message here"/>
            </Form.Group>

            <Button type="submit" variant="outline-light">
                Send Message
            </Button>
        </Form>
        <br/><br/>
        <Footer />
        </div>

        </>
        
        
    )
} 

export default Contactme;