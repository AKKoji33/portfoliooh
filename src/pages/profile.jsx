import React from "react";
import NavbarZ from "../component/navbar"
import Bottom from "../component/bottom"
import Footer from "../component/footer"
import "./profile.css";
import pic from "./cart.png";


function Profile() {


    return (
      <>
      <NavbarZ />
       <div className="main-content">
        <div>
        <div className='profile-container'>
          <img src={pic} className="pic" alt="profil"/>
          <div className="pic-text">
            <h2>About Me</h2>
            <hr/>
            <p> Random Text
            </p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic distinctio quasi officia nam ut, repudiandae eaque quia! Magni ratione temporibus iusto sit numquam, dolore facere eos, perferendis ullam modi ea?
            </p>
          </div>
        </div>
        <Bottom />
        <br/>
        <Footer /> 
        <div>
        
        </div>
        </div>
      
      </div> 
      
      </>
    )
}

export default Profile;