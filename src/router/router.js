import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Landing from "../pages/Landing";
import Profile from "../pages/profile";
import MyContact from "../pages/contact";


const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Landing />
        </Route>
        <Route path="/profile" exact>
          <Profile />
        </Route>
        <Route path="/contact" exact>
          <MyContact />
        </Route>
      </Switch>
    </Router>
  );
};

export default Routes;