import React from "react";
import "./bottom.css";
import {FiLinkedin, FiGithub, FiGitlab, FiFacebook} from 'react-icons/fi';

function Bottom() {
    return (
        <div className="bot-container">
            <div className="icon">
                <a href="https://www.facebook.com/raburabu.01182015/">
                <FiFacebook/></a>
                </div>

            <div className="icon">
                <a href="https://www.linkedin.com/in/atta-kharisma-851130135/">
                <FiLinkedin/></a>
                </div>
            
            <div className="icon">
                <a href="https://gitlab.com/AKKoji33">
                <FiGitlab/></a>
                </div>
            
            <div className="icon">
                <a href="https://github.com/Kris-jvs">
                <FiGithub/></a>
                </div>
        </div>
    );
}

export default Bottom; 