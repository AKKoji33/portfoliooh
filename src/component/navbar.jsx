import React from "react";
import "./navbar.css";
import { Navbar, Nav } from "react-bootstrap";

function NavbarZ() {
    return (
        <div className="navbar-container">
            <Navbar>
            <Nav className="navroute">
            <Nav.Link style={{color: "darkred"}}href="/">Home</Nav.Link>
            <Nav.Link style={{color: "darkred"}} href="/profile">About me</Nav.Link>
            <Nav.Link style={{color: "darkred"}} href="/contact">Contact</Nav.Link>
            </Nav>
            </Navbar>
        </div>
        
    );
}

export default NavbarZ; 