import React from "react";
import "./footer.css";

function Footer() {
    return (
        <div className="footer">
            <p>Copyright &copy; 2021 Atta</p>
        </div>
    );
}

export default Footer; 